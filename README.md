﻿# Cursor Tracking Tool #

Features:
---------
* Add just one script to any HTML page
* Chat integration
* Track user behaviour on your webpage
* Store user data
* Visualize user behaviour

Default admin accounts:
---------
Username: Johan

Email: johan.vannieuwenhuyse@howest.be


Username: Steven

Email: steven.verborgh@howest.be


Username: Axel

Email: axel.jonckheere@student.howest.be


Unit tests:
---------
Unit tests are in the spec folder using [node-jasmine](https://github.com/jasmine/jasmine-npm "Node jasmine").

Required:
---------
You'll need to add some files to make this tool work properly, in the "params" folder you'll need to add two .json files and
fill in your own data:

### dbparams.json ###

    { "username": "name", "password":  "pass", "host":"localhost", "port": "56727", "dbname": "nameofyourdb"  }


__parameters:__

* username: Your username to login to your MongoDb
* password: The password to login to your MongoDb
* host:     The server where your MongoDb is hosted
* port:     The port of the server where your MongoDb is hosted
* dbname:   The name of your MongoDb database

### encrypt.json ###

    {"algorithm":"aes256","key":"xxxxxxxxxxxx"}


__parameters:__

* algorithm: The algorithm used by Node.js' crypto module, e.g.: aes256
* key:       Your random encryption key