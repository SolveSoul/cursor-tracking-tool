﻿var chat = (function () {
    
    var server = require("./server"),
        db = require("./dbconnect.js"),
        io;
    
    var init = function (socketio) {
        io = socketio;
        io.on("connection", chatEvents);
    };
    
    var chatEvents = function (socket) {
        
        socket.on("chat-message", function (msg) {
            io.emit("chat-message", msg);
            db.insertMessage(msg);
            console.log(msg);
        });
        
        socket.on("update-typing-status", function (status) {
            io.emit("update-typing-status", status);
        });

    };
    
    return {
        init: init
    };    

})();
module.exports = chat;