﻿//add eventlisteners to the DOM
document.addEventListener("DOMContentLoaded", init);

var currentUser = {},
    socket;

//document init
function init() {
    dependencies.init();
}

//dependencies module
var dependencies = (function () {
    
    "use strict";
    
    var done = false;
    
    var init = function () {
        initJsDependencies();
        initCss();
    };
    
    var initJsDependencies = function () {
        
        var scriptTag = "";
        
        //add JQuery dependency if it wasn't added already
        if (typeof $ === "undefined") {
            scriptTag = document.createElement("script");
            scriptTag.addEventListener("load", initBootstrap);
            scriptTag.src = "https://code.jquery.com/jquery-2.1.1.min.js";
            document.body.appendChild(scriptTag);
        } else if ((typeof $().modal != "function")) {
            initBootstrap();
        }
        
        //add socket.io dependency, without htmlBuilder for the addEventListener to work properly
        if (typeof io !== "function") {
            scriptTag = document.createElement("script");
            scriptTag.addEventListener("load", chat.socketInit);
            scriptTag.src = "https://cdn.socket.io/socket.io-1.2.0.js";
            document.body.appendChild(scriptTag);
        }
    };
    
    var initCss = function () {
        
        var htmlBuilder = "";
        
        //add the css to the head of the document
        htmlBuilder = "<link href='./css/chat/chat.min.css' rel='stylesheet' />";
        htmlBuilder += "<link href='//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css' rel='stylesheet' />";
        document.head.innerHTML += htmlBuilder;
    };
    
    var initBootstrap = function () {
        var scriptTag;
        scriptTag = document.createElement("script");
        scriptTag.src = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js";
        document.body.appendChild(scriptTag);
        
        loadApplication();
    };
    
    var loadApplication = function () {
        userUtils.init();
    };
    
    return {
        init: init
    };


})();

//chat module
var chat = (function () {
    
    "use strict";
    
    //fields
    var chatHtmlElement,
        form,
        chatInput,
        messagesBox,
        sendButton,
        minimizeButton,
        statusBar,
        messagesContainer,
        htmlBuilder = "",
        isChatMinimized;
    
    //ctor
    var init = function () {
        initChatHtml();
        enableDisableButton(null);
    };
    
    //methods
    var initChatHtml = function () {
        
        resetHtmlBuilder();
        
        //add the chat itself to the page
        htmlBuilder += "<aside id='tracking-chat' class='chat-read'>";
        htmlBuilder += "<header><span></span>Chat<button id='tracking-minimize' class='chat-read'><img src='./images/minimize.svg' /></button></header>";
        htmlBuilder += "<div id='tracking-messages-container'>";
        htmlBuilder += "<ul id='tracking-messages'></ul>";
        htmlBuilder += "<div id='tracking-status'></div>";
        htmlBuilder += "<form id='tracking-form' action=''>";
        htmlBuilder += "<input id='tracking-m' autocomplete='off' /><button id='tracking-send'>Send</button>";
        htmlBuilder += "</form>";
        htmlBuilder += "</div>";
        htmlBuilder += "</aside>";
        document.getElementsByTagName('body')[0].insertAdjacentHTML("afterbegin", htmlBuilder);
        
        
        //set private vars
        chatHtmlElement = document.getElementById("tracking-chat");
        form = document.getElementById("tracking-form");
        chatInput = document.getElementById("tracking-m");
        messagesBox = document.getElementById("tracking-messages");
        sendButton = document.getElementById("tracking-send");
        minimizeButton = document.getElementById("tracking-minimize");
        statusBar = document.getElementById("tracking-status");
        messagesContainer = document.getElementById("tracking-messages-container");
        
        //add eventlisteners
        chatHtmlElement.addEventListener("click", readChatMsg);
        form.addEventListener("submit", sendChatMsg);
        minimizeButton.addEventListener("click", minimizeChat);
        chatInput.addEventListener("keyup", sendIsTyping);
    };
    
    var socketEventInit = function () {
        //init socket.io
        socket = io();
        
        //add socket eventlistener(s)
        socket.on("chat-message", appendChatMessage);
        socket.on("disconnect", disconnectUser);
        socket.on("update-typing-status", updateTypingStatus);
        socket.on("syncuser", syncUserFromServer);
        socket.on("play-capture", cursortracker.playCursor);
        socket.on("initcursor", cursortracker.addCursor);
        socket.on("initmultiplecursors", cursortracker.addMultipleCursors);
        socket.on("remove cursor", cursortracker.removeCursor)
    };
    
    //eventlisteners
    var sendChatMsg = function (e) {
        
        e.preventDefault();
        
        //send message to NodeJS
        socket.emit("chat-message", createChatMessage(chatInput.value, currentUser));
        
        //reset "is typing" status
        socket.emit("update-typing-status", { message: "", user: currentUser });
        
        //clear inputfield
        chatInput.value = "";
        enableDisableButton(null);

    };
    
    var minimizeChat = function (e) {
        
        if (!isChatMinimized) {
            $(chatHtmlElement).animate({ "height": "35px" });
            isChatMinimized = true;
        } else {
            $(chatHtmlElement).animate({ "height": "350px" });
            isChatMinimized = false;
        }

    };
    
    var readChatMsg = function (e) {
        chatHtmlElement.className = "chat-read";
        minimizeButton.className = "chat-read";
    };
    
    var syncUserFromServer = function (user) {
        if (user.admin) {
            chat.init();
        } else { 
            cursortracker.addListener();
        }
        delete user["admin"];
        currentUser = user;
    };

    var disconnectUser = function () {
        window.alert("You were disconnected or the server has gone down.");
    };
    
    var appendChatMessage = function (msg) {
        
        //reset htmlBuilder
        resetHtmlBuilder();
        
        //create html
        if (msg.user.id === currentUser.id) {
            htmlBuilder += "<li class='current-user'>";
            htmlBuilder += "<span>" + msg.message + "<\span>";
            htmlBuilder += "<img src='" + msg.user.avatar + "' title='" + msg.user.username + "' />"
        } else {
            htmlBuilder += "<li class='other-user'>";
            chatHtmlElement.className = "chat-received";
            minimizeButton.className = "chat-received";
            htmlBuilder += "<img src='" + msg.user.avatar + "' title='" + msg.user.username + "' />"
            htmlBuilder += "<span>" + msg.message + "<\span>";
        }
        htmlBuilder += "</li>";
        
        //append html
        messagesBox.innerHTML += htmlBuilder;
        
        //scroll to bottom
        messagesBox.scrollTop = messagesBox.scrollHeight;
        
        console.log(currentUser);
        console.log(msg);
    };
    
    var sendIsTyping = function (e) {
        
        enableDisableButton();
        
        var status = {
            user: currentUser,
            message: chatInput.value
        };
        
        socket.emit("update-typing-status", status);

    };
    
    var updateTypingStatus = function (data) {
        if (data.message !== "") {
            if (currentUser.id !== data.user.id) {
                resetHtmlBuilder();
                
                htmlBuilder += "<span>" + data.user.username + " is typing</span>";
                htmlBuilder += "<img src='./images/typing.gif'></img>";
                statusBar.innerHTML = htmlBuilder;
            }
        } else {
            statusBar.innerHTML = "";
        }

    };
    
    //helper methods
    var resetHtmlBuilder = function () {
        htmlBuilder = "";
    };
    
    var createChatMessage = function (msg, usr) {
        return {
            message: msg,
            user: usr,
            datestamp: new Date().toString()
        };
    };
    
    var enableDisableButton = function () {
        
        if (chatInput.value === "") {
            sendButton.disabled = true;
            sendButton.className = "btn-disabled";
        } else {
            sendButton.disabled = false;
            sendButton.className = "btn-enabled";
        }

    };
    
    //make public
    return {
        init: init,
        socketInit: socketEventInit,
        user: currentUser
    };

})();

//userUtils module
var userUtils = (function () {
    
    "use strict";
    
    //fields
    var htmlBuilder = "",
        modal,
        txtEmail,
        txtUsername;
    
    //init
    var init = function () {
        
        //init html
        initHtml();
        
        //just in case JQuery isn't loaded
        jQuery(document).ready(function ($) {

            if (!localStorage.user) {
                //set vars
                modal = $("#user-credentials");
                txtEmail = document.getElementById("email");
                txtUsername = document.getElementById("username");
                
                //show modal
                modal.modal("toggle");
                $("#modal-ok").on("click", getUserCredentials);
                
            } else {
                currentUser = jQuery.parseJSON(localStorage.user);
                socket.emit("usercredentials", currentUser);
            }
            
        });
    };
    
    //methods
    var getUserCredentials = function () {
        
        if (txtEmail.value !== "" || txtUsername.value !== "") {
            currentUser.email = txtEmail.value;
            currentUser.username = txtUsername.value;
            
            modal.modal("hide");
            
            //send user credentials to server
            socket.emit("usercredentials", currentUser);

            //save user locally
            localStorage.user = JSON.stringify(currentUser);
        }
    };
    
    var initHtml = function () {
        resetHtmlBuilder();
        
        htmlBuilder += "<div class='modal fade' id='user-credentials' role='dialog'>";
        htmlBuilder += "<div class='modal-dialog'>";
        htmlBuilder += "<div class='modal-content'>";
        htmlBuilder += "<div class='modal-header'>";
        htmlBuilder += "<h4 class='modal-title'>Login to CursorTracker</h4>";
        htmlBuilder += "</div>";
        htmlBuilder += "<div class='modal-body'>";
        htmlBuilder += "<form>";
        htmlBuilder += "<label for='username'>Username:</label>";
        htmlBuilder += "<input class='form-control' type='text' id='username' name='username' required/>";
        htmlBuilder += "<label for='email'>Email:</label>";
        htmlBuilder += "<input class='form-control' type='email' id='email' name='email' required/>";
        htmlBuilder += "</form>";
        htmlBuilder += "</div>";
        htmlBuilder += "<div class='modal-footer'>";
        htmlBuilder += "<button id='modal-ok' type='button' class='btn btn-primary'>Ok</button>";
        htmlBuilder += "</div>";
        htmlBuilder += "</div>";
        htmlBuilder += "</div>";
        htmlBuilder += "</div>";
        
        document.getElementsByTagName('body')[0].insertAdjacentHTML("afterbegin", htmlBuilder);
        
    };
    
    //helper methods
    var resetHtmlBuilder = function () {
        htmlBuilder = "";
    };
    
    var removeUserCredentials = function () {
        localStorage.removeItem("user");
    };

    return {
        init: init
    };

})();

var cursortracker = (function () {
    
    "use strict";

    var mouseMovements = [],
        isCapturing = false,
        activeCursors = [];

    var addListener = function () {
        document.addEventListener("mousemove", captureMove);
    };
    
    var addMultipleCursors = function (users) {
        for (var i = 0; i < users.length; i++) { 
            addCursor(users[i]);
        }
    };

    var addCursor = function (data) {
        var cursorImg = new Image(),
            oCursor = {};

        cursorImg.className = "tracking-cursor";
        cursorImg.id = data.id;
        cursorImg.onload = function () {
            document.body.appendChild(cursorImg);
        };
        cursorImg.src = "./images/cursor.svg ";

        oCursor.id = data.id;
        oCursor.cursor = cursorImg;
        activeCursors.push(oCursor);
    };

    var captureMove = function (e) {
        var tempX = 0,
            tempY = 0,
            action = {},
            timer;
            
        isCapturing = true;

        tempX = e.pageX;
        tempY = e.pageY;
        
        if (tempX < 0) { tempX = 0; }
        if (tempY < 0) { tempY = 0; }
        
        action.x = tempX;
        action.y = tempY;

        mouseMovements.push(action);
        
        clearTimeout(timer);
        timer = setTimeout(stopCapture, 500);

        return true;
    };
    
    var stopCapture = function () {

        if (isCapturing){
            
            var result = {};
            result.movements = mouseMovements;
            result.user = currentUser;
           
            socket.emit("send-capture", mouseMovements);
            mouseMovements = [];
            isCapturing = false;
        }

    };
    
    var playCursor = function (data) {
        if (activeCursors.length > 0) {
            var index = activeCursors.map(function (e) { return e.id; }).indexOf(data.user.id);
            activeCursors[index].cursor.style.left = data.cursor[0].x + "px";
            activeCursors[index].cursor.style.top = data.cursor[0].y + "px";
        }
    };

    var removeCursor = function (id) {
        var index = activeCursors.map(function (e) { return e.id; }).indexOf(id);
        activeCursors.splice(index);
        $("#"+id).remove();
    };

    return {
        addListener: addListener,
        addCursor: addCursor,
        addMultipleCursors: addMultipleCursors,
        playCursor: playCursor,
        removeCursor: removeCursor
    };

})();