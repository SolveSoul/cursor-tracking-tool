﻿var userUtils = (function () {
    
    var gravatar = require("gravatar"),
        userList = [],
        db = require("./dbconnect.js");

    var addUser = function (user, callback) {
        
        //get the gravatar for the user
        user.avatar = getGravatar(user);
        
        //insert the user into the database
        db.insertUser(user, function (result) { 
            return callback(result);
        });
    };
    
    var connectUser = function (user) { 
        userList.push(user);
    };

    var disconnectUser = function (socketid) {
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].socket === socketid) {
                userList.splice(i, 1);
                break;
            }
        }
        console.log("user disconnected with id: " + socketid);
    };
    
    var getUserById = function (id) {
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].id === id) {
                return userList[i];
            }
        }
    };
    
    var getUserByEmail = function (email) { 
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].email === email) {
                return userList[i];
                break;
            }
        }
    };
    
    var getUserBySocketId = function (socketid) { 
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].socket === socketid) {
                delete userList[i]["admin"];
                return userList[i];
                break;
            }
        }
    };

    var getUsers = function () {
        return userList;
    };
    
    var getAllNormalUsers = function () {
        var result = [];
        for (var i = 0; i < userList.length; i++) {
            if (!userList[i].admin) { 
                result.push(userList[i]);
            }
        }
        return result;
    };

    var getGravatar = function (user) {
        if (user.email) {
            return gravatar.url(user.email, {s: "25"}, true);
        }
    };

    return {
        addUser: addUser,
        disconnectUser: disconnectUser,
        connectUser: connectUser,
        getUserById: getUserById,
        getUserByEmail: getUserByEmail,
        getUserBySocketId: getUserBySocketId,
        getUsers: getUsers,
        getAllNormalUsers: getAllNormalUsers
    };

}());
module.exports = userUtils;