﻿var encryptUtils = (function () {
    
    var crypto = require("crypto"),
        assert = require("assert"),
        fs = require("fs"),
        log = require("./log"),

        params = require("./params/encrypt.json"),
        cipher,
        decipher;
    
    var init = function () {
        cipher = crypto.createCipher(params.algorithm, params.key);
        decipher = crypto.createDecipher(params.algorithm, params.key);
    };
    
    var encrypt = function (data) {
        return cipher.update(data, "utf8", "hex") + cipher.final("hex");
    };
    
    var decrypt = function (data) {
        return decipher.update(encrypted, "hex", "utf8") + decipher.final("utf8");
    };
    
    return {
        init: init,
        encrypt: encrypt,
        decrypt: decrypt
    };

})();
module.exports = encryptUtils;