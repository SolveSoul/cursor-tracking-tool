﻿//run from root with command: jasmine-node spec --verbose

var db = require("../dbconnect");

describe("database test setup", function () { 

    beforeEach(function () { 
        db.connect();    
    });

    afterEach(function () { 
        db.disconnect();
    });

    describe("insert a new user", function () {
        
        it("should insert a new user", function () {
            var user = {
                id: "unittestid",
                username: "unit test user",
                email: "jasmine@node.js"
            };
            
            db.insertUser(user, function (result, rowsAffected) {
                expect(rowsAffected == 1).toBeTruthy();
                expect(typeof result).toBe("user");
            });

        });

        it("should get a valid user", function () {
            db.getUserByEmail("jasmine@node.js", function (result) {
                expect(result).toBeTruthy();
                expect(typeof result).not.toBe("undefined");
            });
        });

        it("should insert a new message", function () {
            var msg = {
                user: {
                    id: "unittestid",
                    username: "unit test user",
                    email: "jasmine@node.js"
                },
                msg: "this is a test message"
            };
        
            db.insertMessage(msg, function (result, rowsAffected) {
                expect(result).toBeTruthy();
                expect(rowsAffected == 1).toBeTruthy();
            });

        });

    });



});