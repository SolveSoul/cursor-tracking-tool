﻿var log = require("../log"),
    fs = require("fs");

describe("logging a message", function () {
    it("should log a message in a file", function () {
        log.write("error #1337", "An test error occurred", function () {
            
            var filename = "./" + new Date().toDateString() + ".log";
            
            fs.readFile(filename, function (err, data) {
                expect(err).toBeFalsy();
                expect(data.indexOf("error #1337") > 0).toBeTruthy();
            });
        });
    });
});