﻿var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    SeedSchema = new Schema({
        seeded: Boolean
    });
module.exports = SeedSchema;