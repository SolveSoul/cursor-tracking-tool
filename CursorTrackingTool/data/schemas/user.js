﻿var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    UserSchema = new Schema({
        username: { type: String, unique: true },
        email: { type: String, required: true },
        avatar: { type: String, default: "./images/avatar.png" },
        admin: { type: Boolean, default: false },
        createdOn: { type: Date, default: Date.now }
    });
module.exports = UserSchema;