﻿var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    MessageSchema = new Schema({
        _user: { type: Schema.Types.ObjectId, ref: "User" },
        msg: String,
        datestamp: {type: Date, default: Date.now }
    });
module.exports = MessageSchema;