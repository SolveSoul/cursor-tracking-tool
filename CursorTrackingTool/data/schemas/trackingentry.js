﻿var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    TrackingEntrySchema = new Schema({
        _user: [{ type: Schema.Types.ObjectId, ref: "User" }],
        cursor: [{ x: Number, y: Number }]
});