﻿var mongoose = require("mongoose");
var SeedSchema = require("../schemas/seed");
var Seed = mongoose.model("Seed", SeedSchema);
module.exports = Seed;