﻿var mongoose = require("mongoose");
var TrackingEntrySchema = require("../schemas/trackingentry");
var User = mongoose.model('TrackingEntry', TrackingEntrySchema);
module.exports = TrackingEntry;