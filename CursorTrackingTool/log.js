﻿var log = (function () {
    
    var fs = require("fs"),
        extension = ".log";
    
    var write = function (err, message, callback) {
        
        var filename = "./" + new Date().toDateString() + extension,
            errormessage = errormsg(err, message);
        
        fs.appendFile(filename, errormessage, function (err) {
            if (err) { console.log(err); }

            if (typeof callback !== "undefined") { 
                callback(message);
            }

        });

    };
    
    var errormsg = function (err, message) {
        return "[" + new Date().toLocaleTimeString() + "]" + message + ": " + err + "\r\n";
    };
    
    return {
        write: write
    }


})();
module.exports = log;