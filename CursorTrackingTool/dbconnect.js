﻿var dbconnect = (function () {
    
    var mongoose = require("mongoose"),
        connection,
        log = require("./log"),
        fs = require("fs"),

        seedSchema = require("./data/schemas/seed"),
        seedModel = mongoose.model("seed", seedSchema),
        userSchema = require("./data/schemas/user"),
        userModel = mongoose.model("user", userSchema),
        messageSchema = require("./data/schemas/message"),
        messageModel = mongoose.model("message", messageSchema),
        
        params = require("./params/dbparams.json"),
        connectionString = "";
    
    //setup connection
    var connect = function () {
        connectionString = getConnectionString(params);
        connection = mongoose.connect(connectionString);
    };
    
    var disconnect = function () {
        mongoose.disconnect();
    };
    
    //connect to the database
    mongoose.connection.on("open", function (err) {
        
        if (err) { log.write(err, "Couldn't connect to MongoDB"); }
        
        console.log("Successfully connected to: \n" + connectionString.replace(params.password, "xxxxxxxx"));
        
        //seed the database
        //seedDb();
    });
    
    //helper methods
    var getConnectionString = function (params) {
        return "mongodb://" + params.username + ":" + params.password + "@" + params.host + ":" + params.port + "/" + params.dbname;
    };
    
    //CRUD Methods
    var getUserById = function (user, callback) {
        userModel.findOne({ "id": user.id }, function (err, result) {
            if (err) {
                log.write(err, "Query failed");
            }
            
            if (result === null) {
                log.write("", "Couldn't find user");
            }
            
            callback(convertMongooseUserToUser(result));

        });
    };
    
    var getUserByEmail = function (user, callback) {
        userModel.findOne({ "email": user.email }, function (err, result) {
            if (err) {
                log.write(err, "Query failed");
            }
            
            if (result === null) {
                log.write("", "Couldn't find user");
            }
            
            callback(convertMongooseUserToUser(result));

        });
    };
    
    var insertUser = function (user, callback) {
        var insertUser = convertUserToMongooseUser(user);
        insertUser.save(function (err, newUser, rowsAffected) {
            if (err) {
                log.write(err);
            }
            
            if (typeof callback !== "undefined") {
                callback(newUser, rowsAffected);
            }

        });
    };
    
    var insertMessage = function (msg, callback) {
        var insertMsg = convertMessageToMongooseMessage(msg);
        insertMsg.save(function (err, newMsg, rowsAffected) {
            if (err) {
                log.write(err);
            }
            
            if (typeof callback !== "undefined") {
                callback(newMsg, rowsAffected);
            }

        });
    };
    
    //converters
    var convertUserToMongooseUser = function (user) {
        return new userModel({
            _id: mongoose.Types.ObjectId(user.id),
            username: user.username,
            email: user.email,
            avatar: user.avatar
        });
    };
    
    var convertMongooseUserToUser = function (mongUser) {
        if (mongUser !== null) {
            var user = {};
            user.id = mongUser._doc._id.toString();
            user.email = mongUser._doc.email;
            user.avatar = mongUser._doc.avatar;
            user.username = mongUser._doc.username;
            user.admin = mongUser._doc.admin;
            return user;
        } else {
            return null;
        }

    };
    
    var convertMessageToMongooseMessage = function (msg) {
        return new messageModel({
            _user: convertUserToMongooseUser(msg.user),
            msg: msg.message,
            datestamp: msg.datestamp
        });
    };
    
    //seed method
    var seedDb = function () {
        
        //check if the database was seeded or not
        getSeedSchema(function (seed) {
            //not seeded => seed the database
            if (seed.length === 0) {
                
                //seed db with admins
                var johan = new userModel({
                    username: "Johan",
                    email: "johan.vannieuwenhuyse@howest.be",
                    admin: true
                });
                johan.save(function (err) { if (err) { log.write(err, "couldn't seed the database"); } });
                
                var steven = new userModel({
                    username: "Steven",
                    email: "steven.verborgh@howest.be",
                    admin: true
                });
                steven.save(function (err) { if (err) { log.write(err, "couldn't seed the database"); } });
                
                var axel = new userModel({
                    username: "Axel",
                    email: "axel.jonckheere@student.howest.be",
                    admin: true
                });
                axel.save(function (err) { if (err) { log.write(err, "couldn't seed the database"); } });
                
                //save the seed so the db will be seeded only once
                var updateSeed = new seedModel({ seeded: true });
                updateSeed.save(function (err) {
                    if (err) {
                        log.write(err, "couldn't seed the database");
                    } else {
                        console.log("Database succesfully seeded");
                    }
                });
            }
        });
    };
    
    var getSeedSchema = function (callback) {
        seedModel.find({}, function (err, result) {
            if (err) {
                log.write(err, "Query failed");
            }
            callback(result);
        });
    };
    
    return {
        connect: connect,
        disconnect: disconnect,
        insertUser: insertUser,
        getUserByEmail: getUserByEmail,
        getUserById: getUserById,
        insertMessage: insertMessage
    };

})();
module.exports = dbconnect;