﻿var server = (function () {
    
    var app = require("express")(),
        http = require("http").Server(app),
        io = require("socket.io")(http),
        path = require("path"),
        express = require("express"),
        userUtils = require("./userutils"),
        chat = require("./chat"),
        encryptUtils = require("./encryptutils"),
        db = require("./dbconnect.js");
    
    
    //init function
    var init = function (httpPort) {
        //init server
        httpListen(httpPort);
        
        //make 'public' dir really public
        app.use(express.static(path.join(__dirname, '/public')));
        
        app.get('/', function (req, res) {
            res.sendFile(__dirname + '/public/chat.html');
        });
        
        chat.init(io);
        db.connect();
        encryptUtils.init();
    };
    
    //functions
    var httpListen = function (httpPort) {
        http.listen(httpPort, function () {
            console.log('server listening on localhost:' + httpPort);
        });
    };
    
    //server eventlisteners
    io.on("connection", function (socket) {
        
        //connect the user or add a new user
        socket.on("usercredentials", function (user) {
            
            db.getUserByEmail(user, function (currentUser) {
                
                if (!currentUser) {
                    userUtils.addUser(user, function (result) {
                        console.log("user connected with id: " + result.id);
                        result.socket = socket.id;
                        userUtils.connectUser(result);
                        
                        if (result.admin) {
                            socket.join("adminroom");
                        } else {
                            socket.join("userroom");
                            io.sockets.in("adminroom").emit("initcursor", result);
                        }
                        
                        socket.emit("syncuser", userUtils.getUserById(result.id));
                    });
                } else {
                    console.log("user connected with id: " + currentUser.id);
                    currentUser.socket = socket.id;
                    userUtils.connectUser(currentUser);
                    
                    if (currentUser.admin) {
                        socket.join("adminroom");
                        io.sockets.in("adminroom").emit("initmultiplecursors", userUtils.getAllNormalUsers());
                    } else {
                        socket.join("userroom");
                        io.sockets.in("adminroom").emit("initcursor", currentUser);
                    }
                    
                    socket.emit("syncuser", userUtils.getUserById(currentUser.id));
                }
            
            });

        });
        
        socket.on("send-capture", function (data) {
            var oCursor = {};
            oCursor.cursor = data;
            oCursor.user = userUtils.getUserBySocketId(socket.id);
            io.sockets.in("adminroom").emit("play-capture", oCursor);
        });
        
        socket.on("disconnect", function () {
            var user = userUtils.getUserBySocketId(socket.id);
            if (typeof user !== "undefined") { 
                io.sockets.in("adminroom").emit("remove cursor", user.id);
            }
            userUtils.disconnectUser(socket.id);
        });

    });

    //make public
    return {
        init: init
    };
})();
module.exports = server;